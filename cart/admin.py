from django.contrib import admin
from .models import Cart, CartMenu, User

admin.site.register(Cart)
admin.site.register(CartMenu)
admin.site.register(User)