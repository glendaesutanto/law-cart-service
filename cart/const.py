BASE_URL = "https://cart-service-law.herokuapp.com/"
RESTAURANT_URL = "https://restaurant-service-law.herokuapp.com/restaurant-data"
AUTH_URL = "https://law-b7-auth.herokuapp.com/api"
ORDER_URL = "https://order-service-law.herokuapp.com"

MENU_URL = f"{RESTAURANT_URL}/menu"

CREATE_ORDER_URL = f"{ORDER_URL}/order/create"

LOG_URL = "https://logs-01.loggly.com/inputs/c33818a3-eb2d-4b4f-8d89-6dae5e3993c1/tag/http"

def get_restaurant_obj_url(restaurant_id):
    return f"{RESTAURANT_URL}/{restaurant_id}"

def get_menu_obj_url(menu_id):
    return f"{MENU_URL}/{menu_id}"

def get_user_data_url():
    return f"{AUTH_URL}/user"

VIEWS_URL_MAP = {
    'get_menu_list': {
        'url': '',
        'method': 'GET'
    },
    'create_new_cart': {
        'url': 'create',
        'method': 'POST'
    },
    'add_menu_to_cart': {
        'url': 'add/menu/',
        'method': 'POST'
    },
    'set_quantity': {
        'url': 'set-menu-qty/',
        'method': 'PUT'
    },
    'delete_menu': {
        'url': 'delete/menu/',
        'method': 'DELETE'
    },
    'set_status': {
        'url': 'set-status',
        'method': 'PUT'
    }
}