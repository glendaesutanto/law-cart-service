from django.db import models


class User(models.Model):
    email = models.CharField(max_length = 256, null = True, blank = True)
    username = models.CharField(max_length = 256, null = True, blank = True)
    user_id = models.IntegerField(null = True, blank = True)

    def __str__(self):
        return f"{self.email}"


class Cart(models.Model):
    status = models.CharField(max_length = 256, null = True, blank = True)
    user = models.ForeignKey(User, on_delete = models.CASCADE)

    def __str__(self):
        return f"{self.pk}: {self.user} - {self.status}"


class CartMenu(models.Model):
    cart = models.ForeignKey(Cart, on_delete = models.CASCADE)
    restaurant_id = models.IntegerField(null = True, blank = True)
    menu_id = models.IntegerField(null = True, blank = True)
    quantity = models.IntegerField(null = True, blank = True)
    menu_name = models.CharField(max_length = 256, null = True, blank = True)
    restaurant_name = models.CharField(max_length = 256, null = True, blank = True)
    price = models.PositiveIntegerField(null = True, blank = True)

    def __str__(self):
        return f"{self.cart} [[{self.menu_name}({self.quantity}) - {self.restaurant_name}]]"
