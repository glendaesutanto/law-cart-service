from . import views
from django.urls import path

app_name = 'cart'

urlpatterns = [
    path('', views.get_menu_list, name='get_menu_list'),
    path('create', views.create_new_cart, name='create_new_cart'),
    path('add/menu/<int:menu_id>', views.add_menu_to_cart, name='add_menu_to_cart'),
    path('set-menu-qty/<int:menu_id>', views.set_quantity, name='set_quantity'),
    path('delete/menu/<int:menu_id>', views.delete_menu, name='delete_menu'),
    path('set-status', views.set_status, name='set_status'),
]
