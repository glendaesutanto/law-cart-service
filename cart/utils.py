import json
import requests
import os

from django.http import JsonResponse
from .const import *

def json_response(content, status):
    return JsonResponse(content, safe = False, status = status)


def get_restaurant_obj(restaurant_id):
    result = requests.get(get_restaurant_obj_url(restaurant_id))
    if result.status_code == 200:
        return json.loads(result.content)["data"]


def get_menu_obj(menu_id):
    result = requests.get(get_menu_obj_url(menu_id))
    if result.status_code == 200:
        return json.loads(result.content)["data"]


def send_logs(view_name, payload, additional_headers, additional_url_path, response, user):
    if os.environ.get('IS_PRODUCTION_ENV') == 'False':
        return

    method = VIEWS_URL_MAP[view_name]["method"]
    url = VIEWS_URL_MAP[view_name]["url"]

    payload = {
        'service': 'CART_SERVICE',
        'method': method,
        'endpoint': f"{BASE_URL}/{url}{additional_url_path}",
        'payload': payload,
        'additional_headers': additional_headers,
        'response_data': json.loads(response.content),
        'response_status_code': response.status_code,
        'user_email': user.email,
    }
    print('log payload:',payload)

    result = requests.post(LOG_URL, data=json.dumps(payload))
    if result.status_code != 200:
        print("Cart service failed sending logs :(")

    print("Cart service successfully sending logs!")
