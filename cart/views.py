import requests
import json

from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view

from .models import Cart, CartMenu, User
from .const import get_user_data_url
from .utils import *
    

def get_or_create_user(user_data):
    user_email = user_data['email']
    user_username = user_data['username']
    user_user_id = user_data['user_id']

    user = User.objects.filter(email=user_email, username=user_username).first()
    if user is None:
        user = User.objects.create(email=user_email, username=user_username, user_id=user_user_id)

    return user


@api_view(["POST"])
def get_menu_list(request):
    user_data = request.data['user']
    print("user_data:", user_data)
    user = get_or_create_user(user_data)

    active_cart = Cart.objects.filter(status='active', user=user).first()

    if active_cart is None:
        response = json_response({'error': 'User does not have any active cart.'}, status.HTTP_400_BAD_REQUEST)
        send_logs('get_menu_list', request.data, {}, '', response, user)
        return response

    menu_list = active_cart.cartmenu_set.all()

    menu_list_response = []

    for menu in menu_list:
        menu_list_response.append({
            'id': menu.pk,
            'restaurant': {
                'id': menu.restaurant_id,
                'name': menu.restaurant_name
            },
            'menu': {
                'id': menu.menu_id,
                'name': menu.menu_name,
                'price': menu.price
            },
            'quantity': menu.quantity,
            'cart': menu.cart.pk
        })

    response = json_response({
        'id': active_cart.pk,
        'cart_menu_list': menu_list_response,
        'status': active_cart.status
    }, status.HTTP_200_OK)

    send_logs('get_menu_list', request.data, {}, '', response, user)

    return response


@api_view(['POST'])
def create_new_cart(request):
    user_data = request.data['user']
    user = get_or_create_user(user_data)

    active_cart = Cart.objects.filter(status='active', user=user).first()

    if active_cart is not None:
        response = json_response({'error': f'Can not create new cart because this user still has active cart with id {active_cart.pk}'}, status.HTTP_400_BAD_REQUEST)
        send_logs('create_new_cart', request.data, {}, '', response, user)
        return response

    new_cart = Cart.objects.create(
        status='active',
        user=user
    )

    response = json_response({'message': 'Successful!', 'cart_id': new_cart.pk}, status.HTTP_201_CREATED)
    send_logs('create_new_cart', request.data, {}, '', response, user)
    return response


@api_view(['POST'])
def add_menu_to_cart(request, menu_id):
    user_data = request.data['user']
    user = get_or_create_user(user_data)

    quantity = request.data['quantity']

    active_cart = Cart.objects.get(status='active', user=user)

    cart_menu = CartMenu.objects.filter(cart=active_cart, menu_id=menu_id).first()
    if cart_menu is not None:
        cart_menu.quantity += quantity
        cart_menu.save()
    else:
        menu_obj = get_menu_obj(menu_id)
        restaurant_id = menu_obj["restaurant"]
        restaurant_obj = get_restaurant_obj(restaurant_id)
        cart_menu = CartMenu.objects.create(
            cart = active_cart,
            restaurant_id = restaurant_id,
            menu_id = menu_id,
            quantity = quantity,
            menu_name = menu_obj["name"],
            restaurant_name = restaurant_obj["name"],
            price = menu_obj["price"]
        )

    response =  json_response({
        'id': cart_menu.pk,
        'restaurant': {
            'id': cart_menu.restaurant_id,
            'name': cart_menu.restaurant_name
        },
        'menu': {
            'id': cart_menu.menu_id,
            'name': cart_menu.menu_name,
            'price': cart_menu.price
        },
        'quantity': cart_menu.quantity,
        'cart': cart_menu.cart.pk
    }, status.HTTP_201_CREATED)
    send_logs('add_menu_to_cart', request.data, {}, menu_id, response, user)
    return response


@api_view(["PUT"])
def set_quantity(request, menu_id):
    user_data = request.data['user']
    user = get_or_create_user(user_data)

    quantity = request.data['quantity']

    active_cart = Cart.objects.get(status='active', user=user)

    cart_menu = CartMenu.objects.get(cart=active_cart, menu_id=menu_id)

    cart_menu.quantity = quantity
    cart_menu.save()

    response = json_response({
        'id': cart_menu.pk,
        'restaurant': {
            'id': cart_menu.restaurant_id,
            'name': cart_menu.restaurant_name
        },
        'menu': {
            'id': cart_menu.menu_id,
            'name': cart_menu.menu_name,
            'price': cart_menu.price
        },
        'quantity': cart_menu.quantity,
        'cart': cart_menu.cart.pk
    }, status.HTTP_200_OK)
    send_logs('set_quantity', request.data, {}, menu_id, response, user)
    return response


@api_view(["POST"])
def delete_menu(request, menu_id):
    user_data = request.data['user']
    user = get_or_create_user(user_data)

    active_cart = Cart.objects.get(status='active', user=user)

    cart_menu = CartMenu.objects.filter(cart=active_cart, menu_id=menu_id).first()

    cart_menu.delete()

    response = json_response({
        'message': f"Successfully deleted menu {menu_id} from cart!"
    }, status.HTTP_200_OK)
    send_logs('delete_menu', request.data, {}, menu_id, response, user)
    return response


@api_view(["PUT"])
def set_status(request):
    user_data = request.data['user']
    user = get_or_create_user(user_data)

    new_status = request.data['status']

    active_cart = Cart.objects.get(status='active', user=user)

    active_cart.status = new_status
    active_cart.save()

    response = json_response({
        'message': f"Successfully updated cart status to {new_status}!"
    }, status.HTTP_200_OK)
    send_logs('set_status', request.data, {}, '', response, user)
    return response


def create_order(cart_id):
    print(f"Cart {cart_id} has been successfully paid! Creating order...")

    # set status cart to paid
    cart = Cart.objects.get(pk=cart_id)
    cart.status = "paid"
    cart.save()

    # create new cart
    Cart.objects.create(
        status='active',
        user=cart.user
    )

    # call create order at order service
    user_id = cart.user.user_id
    menu_per_restaurant = {}

    cart_menu_list = cart.cartmenu_set.all()
    for cart_menu in cart_menu_list:
        if cart_menu.restaurant_id not in menu_per_restaurant:
            menu_per_restaurant[cart_menu.restaurant_id] = [cart_menu.menu_id]
        else:
            menu_per_restaurant[cart_menu.restaurant_id].append(cart_menu.menu_id)

    for restaurant_id in menu_per_restaurant:
        payload = {
            'user_id': user_id,
            'restaurant_id': restaurant_id,
            'menu_id_list': menu_per_restaurant[restaurant_id]
        }

        print(payload)

        result = requests.post(CREATE_ORDER_URL, data=json.dumps(payload), headers={'Content-Type': 'application/json'})
        if result.status_code == 201:
            print(f"Successfully create order for cart {cart_id} and restaurant id {restaurant_id}")
        else:
            print(f"Failed create order for cart {cart_id} and restaurant id {restaurant_id} :(((")
