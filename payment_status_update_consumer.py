import pika

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cartservice.settings")

import django
django.setup()

params = pika.URLParameters("amqp://nlekkhuz:8uhhFH6MhUL0LIZA3ijM6y1QMe7k3EbA@albatross.rmq.cloudamqp.com/nlekkhuz")

from cart.views import create_order

connection = pika.BlockingConnection(params)
channel = connection.channel()

channel.queue_declare(queue='payment_status_update')

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    [cart_id, status] = str(body, 'utf-8').split(":")
    if status == "SUCCESS":
        create_order(cart_id)
    else:
        print("Payment failed. Please try to pay again.")

channel.basic_consume(queue='payment_status_update',
                      auto_ack=True,
                      on_message_callback=callback)

print(' [*] Waiting for messages')
channel.start_consuming()
